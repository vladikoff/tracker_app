package club.casadeballoon.tracker_app;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

import android.app.*;
import android.content.*;
import android.location.GpsStatus.NmeaListener;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.*;
import android.util.Log;


public class MainService extends Service {
	
	PowerManager.WakeLock wakeLock = null;
	LocationManager locationManager = null;

	public class SMSTimerTask extends TimerTask {
		private Context _context;
		
		public SMSTimerTask(Context context) {
			_context = context;
		}
		@Override
		public void run() {
			Log.d("CasaDeBalloon", "SMSTimerTask begin run");
			Utils.sendLocationSMS(_context);
		}
	}
	Timer smsTimer = null;
	
	public class TrackerTimerTask extends TimerTask {
		private Context _context;
		
		public TrackerTimerTask(Context context) {
			_context = context;
		}
		
		@Override
		public void run() {
			Log.d("CasaDeBalloon", "TrackerTimerTask begin run");
			Utils.sendTrackerPing(_context);
		}

	}
	Timer trackerTimer = null;

	public class NMEALogListener implements NmeaListener {
		@Override
		public void onNmeaReceived(long timestamp, String nmea) {
			Utils.appendToFile(Utils.nmeaLogFile, nmea);
		}
	}
	NMEALogListener nmeaListener = null;
	
	public class NoopLocationListener implements LocationListener {
		private Context _context;
		
		public NoopLocationListener(Context context) {
			_context = context;
		}
		
		@Override
		public void onLocationChanged(Location location) {
			Log.d("CasaDeBalloon", "NoopLocationListener onLocationChanged");

			DecimalFormat df = new DecimalFormat("#.########");
			String latitude = df.format(location.getLatitude());
			String longitude = df.format(location.getLongitude());
			df = new DecimalFormat("#.#");
			String altitude = df.format(location.getAltitude());
				
			String gpsStatus = "GPS "+new SimpleDateFormat("HH:mm:ss").format(new Date(location.getTime()))+" Pos: "+latitude+" / "+longitude + " / "+altitude;
			Utils.putStringVal(_context, Utils.gpsStatusKey, gpsStatus);
		}

		@Override
		public void onStatusChanged(String provider, int status, Bundle extras) {
		}

		@Override
		public void onProviderEnabled(String provider) {
		}

		@Override
		public void onProviderDisabled(String provider) {
		}		
	}
	NoopLocationListener noopListener = null;

	@Override
	public IBinder onBind(Intent arg0) {
		return null;
	}

	@Override
	public void onCreate() {
		noopListener = new NoopLocationListener(this);
		nmeaListener = new NMEALogListener();
		super.onCreate();
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		Log.d("CasaDeBalloon", "Starting Service");
		
		PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
		wakeLock = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "CasaDeBalloonTrackerServiceLock");
		wakeLock.acquire();
		
		locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
		locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,
				150, 0, noopListener);
		locationManager.addNmeaListener(nmeaListener);
		smsTimer = new Timer();
		smsTimer.schedule(new SMSTimerTask(this), 5000, 60*1000);
		trackerTimer = new Timer();
		trackerTimer.schedule(new TrackerTimerTask(this), 8000, 20*1000);
		return 0;
	}

	@Override
	public void onDestroy() {
		Log.d("CasaDeBalloon", "Stopping Service");
		smsTimer.cancel();
		smsTimer.purge();
		trackerTimer.cancel();
		trackerTimer.purge();
		locationManager.removeUpdates(noopListener);
		locationManager.removeNmeaListener(nmeaListener);
		wakeLock.release();
		super.onDestroy();
	}
	

}