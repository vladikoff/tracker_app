package club.casadeballoon.tracker_app;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

public class StopActivity extends Activity {

	
	private final Activity me = this;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		AlarmManager m_alarmMgr = (AlarmManager)getSystemService(Context.ALARM_SERVICE);
		
		m_alarmMgr = (AlarmManager)getSystemService(Context.ALARM_SERVICE);
		Intent alarmIntent = new Intent(AlarmReceiver.ALARM_ACTION_NAME);
		PendingIntent alarmPI = PendingIntent.getBroadcast(me, Utils.alarmId, alarmIntent, PendingIntent.FLAG_UPDATE_CURRENT);
		m_alarmMgr.cancel(alarmPI);
        
		Intent serviceIntent = new Intent(me, MainService.class);
	    me.stopService(serviceIntent);

		Utils.putBooleanVal(me, Utils.photoRunningKey, false);
        me.finish();	
    }

}
